## Flags 
Like registers, they hold data.
Flags only hold 1 bit. They are either true or false.
Individual flags are part of a larger register.

| Flag Symbol | Description | 
|--------------|-------------|
| CF | Carry | 
| PF | Parity | 
| ZF | Zero |
| SF | Sign |
| OF | Overflow | 
| AF | Adjust |
| IF | Interrupt Enabled |


## Pointers
Pointers are also registers that hold data.
They "point to" data, meaning, they hold its memory address.

| Pointer Name (64) (32, 16) | Meaning | Description |
|--------------|-------------|---|
| rip (eip,ip)       | Index pointer |  Points to next address to be executed in the control flow. |
| rsp (esp,sp) | Stack pointer | Points to the top address of the stack. |
| rbp (ebp, bp) | Stack base pointer | Points to the bottom of the stack. |


## Control flow
All code runs from top to bottom by default. The direction a program flows is called the control flow.

The rip register holds the address of the next instruction to be executed. After each instruction, it is incremented by 1, making the control flow naturally flow from top to bottom.

## Jumps
Jumps can be used to skip to different parts of code based on labels.
They are used to divert the program flow.
Example: 
```
_start:
	jmp _start
```

## Comparisons
They allow programs to be able to take different paths based on certain conditions.
Comparisons are done on **registers**.
The general format of a comparison is...
```
cmp register, register/value

cmp rax, 23
cmp rax, rbx
```
After making a comparison, certain flags are set.

| cmp a, b|
|----------|---------------------|
| a=b | ZF = 1 |
| a!=b | ZF = 0 |
| - | SF = msb(a-b) |

## Conditional Jumps
After a comparison is made, a conditional jump can be made.
Conditional jumps are based on the status of the flags.
Conditional jumps in code are written just like unconditional jumps however "jmp" is replaced by the symbol for the conditional jump.

| Jump symbol (signed) | Jump symbol (unsigned) | Results of cmp a,b |
|------------------------|--------------------------|---------------------|
| je | - | a = b | 
| jne | - | a != b|
| jg | ja | a > b |
|jge | jae | a >= b |
|jl | jb| a < b|
|jle | jbe | a <= b |\
| jz | - | a = 0 |
|jnz | - | a != 0 |
| jo | - | Overflow occurred | 
|jno | - | Overflow did not occur |
| js | - | Jump if signed |
| jns | - | Jump if not signed |

### Conditional Jump Examples
This code will jump to the address of label `_doThis` if and only if the value in the rax register equals 23.
```
cmp rax, 23
je _doThis
```
This code will jump to the address of label `doThis` if and only if the value in the rax register is greater than the value in the rbx register.
```
cmp rax, rbx
jg _doThis
```


## Registers as Pointers
The default registers can be treated as pointers.
To treat a register as a pointer, surround the register name with square brackets, such as `"rax"` becomes `"[rax]"`.
```
mov rax, rbx
; loads the value in the rbx register into the rax register.

mov rax, [rbx]
; loads the value the rbx register is pointing to into the rax register.
```


## Calls
Calls and jumps are essentially the same.
However when "call" is used, the original position the call was made can be returned to using `ret`.
This is called a **subroutine**.