Math operations are used to mathematically manipulate registers.
The form of a math operation is typically: 
`operation register, value/register`
The first register is the subject of the operation.
Example:
```
add rax, 5
sub rbx, rax
```

## Math Operations List
| Operation Name | Operation Name (Signed) | Description |
|------------------|---------------------------|--------------|
| add a,b | - | a = a+b |
| sub a,b | - | a = a-b |
| mul reg | imul reg | rax = rax * reg |
| div reg | idiv reg | rax = rax/reg |
| neg reg | - | reg = -reg |
| inc reg | - | reg = reg + 1 |
| dec reg | - | reg = reg - 1 |
| adc a,b | - | a = a+b+CF |
| sbb a,b | - | a = a-b-CF |

## Display a Digit
The value in the rax register is incremented by 48. If you check the ASCII chart, 48 is the value of "0" 48+1 is the value of 1 character, etc...

```
section .data
	digit db 0,10

_printRAXDigit:
	add rax, 48
	mov [digit], al
	mov rax, 1
	mov rdi, 1
	mov rsi, digit
	mov rdx, 2
	syscall
	ret
```

`mov [digit], al` 
The lower byte of the rax register is then moved into the memory address "digit". `al` references the least significant byte of the register.
"digit" is actually defined with two bytes, being 0 and 10, a new line character. Since we are only loading the lower byte of the rax register into "digit" it onlhy overwrites the first byte and does not affect the new line character.
```
mov rax, 1
mov rdi, 1
mov rsi, digit
mov rdx, 2
syscall
```
We then print the 2 bytes to the screen. This will display the digit as well as the new line characters since our length is set to 2.

You can use this subroutine to display a digit between 0-9 by loading that digit into the rax register and then calling the subrutine.
```
mov rax, 7
call _printRAXDigit
```


## The Stack
Another way to temporarily store data..
It is called the stack because you *stack* data onto it.
### Terminology
When you add data onto the top of the stack, you **push** data onto the stack.
When you remove data from the top of the stack you **pop** data from the stack.
If you look at the top of the stack without removing or adding anything to it, this is called **_peeking_**.


## Stack Operations
| Operation | Effect |
|-----------|--------|
| `push reg/value` | Pushes a value onto the stack. |
| `pop reg` | Pops a value off the stack and stores it in reg. |
| `mov reg, [rsp]` | Stores the peek value in reg. |

Note: Usually in places where you can use registers, you can also use pointers.
Such as, instead of `pop reg` you can use `pop [label]` to pop a value off the stack directly into a position in memory.