Code:
```asm
section .data  
       text db "Hello, World",10 
  
section .text  
       global _start  
  
_start:  
       mov rax, 1  
       mov rdi, 1  
       mov rsi, text  
       mov rdx, 14  
       syscall  
  
       mov rax, 60  
       mov rdi, 0  
       syscall
```

db stands for "defined bytes". It means that we are going to define **raw** bytes of data to insert into our code.

"Hello world" would be the bytes of data we are defining. Each character is a byte. The 10 is a newline (\\n).

"text" is a name assigned to the address in memory where this data will be located.
Whenever we use "text", when the code is compiled the compiler will determine the actual location in memory of this piece of data and replace all future instances of "text" with that memory address.

## Registers
Registers are a part of the processor which hold memory (temporarily).
In the x86_64 architecture, registers hold 64 bits.
![[Pasted image 20230719184127.png]]

## System Call
A **system call** or **syscall**, is when a program requests a service from the **kernel**.
System calls will differ by operating system because different operating systems use different kernels.
All syscalls have an ID associated with them.
Syscalls also take arguments (a list of inputs).

### System Call Inputs by Register
| Argument | Registers | 
|-----------|------------|
|ID | rax|
| 1 | rdi |
| 2 | rsi |
| 3 | rdx |
| 4 | r10 |
| 5 | r8 |
| 6 | r9 |

### System Call List
| syscall | ID | ARG1 | ARG2 | ARG3|
|--------|---|-------|-------|-------|
|sys_read| 0 | filedescriptor# | buffer$ | count# |
|sys_write| 1 | filedescriptor# | buffer$ | count# |
| sys_open | 2 | filename$ | flags# | mode# |
| sys_close | 3 | filedescriptor# | | | |

(# marks values that have to be numbers and $ marks memory addresses with a name)

## Sections
All x84_64 assembly files have three sections, the ".data" section, the ".bss" section, and the ".text" section.

The data section is where all data is defined before compilation.

The bss section is where data is allocated for future use.

The text section is where the actual code goes.

## Labels
Upon compilation, the compiler will calculate the location in which the label will sit in memory.
Any time the name of the label is used afterwards, that name is replaced by the location in memory by the compiler.
(`start:` is a label)

## The "Start" Label
The `_start` label is essential for all programs.
When your program is compiled and then executed, it is executed first at the location of `_start:`.

If the linker cannot find `_start`, it will throw an error.

## Global
The keyword "global" is used when you want the linker to be able to know the address of a label.
The object file generated will contain a link to every label declared "global".
In this case, we have to declare `_start` as global since it is required for the code to be properly linked.