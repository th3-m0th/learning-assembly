A macro is a single instruction that expands into a predefined set of instructions to perform a particular task.
```
		mov rax, 60
exit  ->mov rdi, 0
		syscall
```

## Defining Macros
```
%macro <name> <argc>
	...
	<macro body>
	...
%endmacro
```
`<argc>` would be the number of arguments the macro will take. Arguments are inputs that can be passed into the macro.

Within the macro body, these inputs are referenced using `%1` for the first input, `%2` for the second, etc.

If args > 1, then a comma is used between inputs.

### "Exit" macro
```
%macro exit 0
	mov rax, 60
	mov rdi, 0
	syscall
%endmacro
```

4:04